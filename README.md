# Site de génération de requete sql

# Description du projet 

Ce site est conçu pour générer des requêtes SQL avec quelques vérifications telles que :

Lorsque l'utilisateur oublie d'entrer une valeur pour un champ, une erreur s'affiche pour lui rappeler d'entrer une valeur pour ce champ.
Si l'utilisateur sélectionne un opérateur avant un attribut, ou s'il sélectionne un opérateur à la fin de la requête, une erreur est générée car cela constitue une syntaxe invalide.
Il y a également d'autres fonctionnalités intégrées :

Le bouton "Annuler la sélection" est actif uniquement lorsqu'un attribut est sélectionné. Ce bouton permet à l'utilisateur d'annuler la sélection des champs à tout moment.
Le bouton "Requête SQL Générée" est actif uniquement lorsque des attributs sont sélectionnés. Il permet à l'utilisateur de générer la requête SQL une fois la sélection effectuée.

# Environnement de développement
Visual studio code

# Exécuter le fichier
index.html