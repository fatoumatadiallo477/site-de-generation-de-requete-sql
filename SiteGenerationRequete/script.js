$(document).ready(function () {
    // Fonction pour rendre le bouton "Annuler la sélection" inactif au départ
    $("#cancel-selection").prop('disabled', true);

    // Fonction pour rendre le bouton "Générer SQL Query" inactif au départ
    $("#generate-query").prop('disabled', true);

    $(".attribute, .operator").draggable({
        helper: "clone",
        revert: "invalid"
    });

    $(".droppable").droppable({
        accept: ".attribute, .operator",
        drop: function (event, ui) {
            var droppedItem = $(ui.draggable).clone();
            var attribute = $(droppedItem).attr('data-attribute');
            var operator = $(droppedItem).attr('data-operator');
            if (attribute) {
                var input = '<input type="text" class="attribute-value" data-attribute="' + attribute + '" placeholder="Value">';
                $(droppedItem).append(input);
            }
            if (operator) {
                var operatorText = operator;
                var operatorElement = $('<div class="operator">' + operatorText + '</div>');
                $(this).append(operatorElement);
                return; 
            }
            $(this).append(droppedItem);
            updateButtons();
        }
    });

    $("#selected-attributes").on('click', '.attribute', function () {
        // Supprimer l'attribut uniquement s'il y a des valeurs à l'intérieur
        var value = $(this).find('.attribute-value').val();
        if (!value) {
            return; 
        }
        $(this).remove();
        updateButtons();
    });

    $("#cancel-selection").click(function () {
        $("#selected-attributes").empty(); 
        $("#sql-query").empty();
        $("#alert").hide(); 
        updateButtons();
    });

    $("#generate-query").click(function () {
        var query = "";
        var prevOperator = "";
        var missingValue = false; 
        var invalidSyntax = false; 
    
        $("#selected-attributes").children().each(function (index) {
            var attribute = $(this).attr('data-attribute');
            var value = $(this).find('.attribute-value').val();
            if (attribute) {
                if (index > 0) {
                    query += " " + prevOperator + " ";
                }
                if (!value) {
                    missingValue = true; 
                    return false; 
                }
                query += attribute + "='" + value + "'";
            } else {
                var operator = $(this).text();
                prevOperator = operator === "ET" ? "AND" : "OR";
            }
        });
    
        // Vérifiez si le dernier élément est un opérateur et ajoutez-le à la requête si nécessaire
        var lastChild = $("#selected-attributes").children().last();
        if (lastChild.hasClass("operator")) {
            var operatorText = lastChild.text();
            query += " " + (operatorText === "ET" ? "AND" : "OR") + " ";
        }
    
        // Afficher une alerte si une valeur d'attribut est manquante
        if (missingValue) {
            $("#alert").show();
            return; 
        }
    
        // Vérifier la syntaxe invalide
        if (query.startsWith("AND") || query.startsWith("OR") || query.endsWith("AND") || query.endsWith("OR")) {
            invalidSyntax = true;
        } else {
            var operatorsCount = (query.match(/AND|OR/g) || []).length;
            var attributesCount = (query.match(/'[^']*'/g) || []).length;
            if (operatorsCount !== attributesCount - 1) {
                invalidSyntax = true;
            }
            if (query.endsWith("AND") || query.endsWith("OR")) {
                invalidSyntax = true;
            }
        }
    
        // Afficher une alerte si la syntaxe est invalide
        if (invalidSyntax) {
            $("#sql-query").text("SQL Query invalid: Syntax error");
            $("#alert").hide(); 
            return; 
        }
    
        // Cacher l'alerte s'il n'y a aucune valeur manquante et si la syntaxe est valide
        $("#alert").hide();
    
        // Procéder à la généraation de la requête
        $("#loading-bar").show();
        setTimeout(function () {
            $("#loading-bar").hide();
            $("#sql-query").text("Génération de la requête SQL: SELECT * FROM table WHERE " + query);
        }, 2000); 
    });

    //  Fonction pour mettre à jour l'état des boutons d'annulation de la sélection et de génération de requête
    function updateButtons() {
        var numSelectedAttributes = $("#selected-attributes").children().length;
        var generateButton = $("#generate-query");
        var cancelButton = $("#cancel-selection");
    
        generateButton.prop('disabled', numSelectedAttributes === 0);
    
        cancelButton.prop('disabled', false);
    
        if (numSelectedAttributes > 0) {
            $("#alert").hide();
        }
    }
});